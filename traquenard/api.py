from place.models import Place
from rest_framework import viewsets, routers
from rest_framework import serializers
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


class UserSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=100)

class PlaceTypeSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100)
    icon = serializers.CharField(max_length=100)


class GeoAdressSerializer(serializers.Serializer):
    lon = serializers.FloatField()
    lat = serializers.FloatField()

class GeoDistanceSerializer(serializers.Serializer):
    lon = serializers.FloatField()
    lat = serializers.FloatField()
    distance = serializers.FloatField()

class NearSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100)
    address = serializers.CharField(max_length=250)
    geoadress = GeoDistanceSerializer()


class PlaceSerializer(serializers.ModelSerializer):

    created_by = UserSerializer()
    place_type = PlaceTypeSerializer()
    geoadress = GeoAdressSerializer()
    likes = serializers.IntegerField(source='likes.count', read_only=True)
    dislikes = serializers.IntegerField(source='dislikes.count', read_only=True)
    near = NearSerializer(source = "get_places_near")
    class Meta:
        model = Place
        fields = ('name',
                  'address',
                  'place_type',
                  'description',
                  'created_by',
                  'likes',
                  'dislikes',
                  'geoadress',
                  'near')
        depth = 1

# ViewSets define the view behavior.
class PlaceViewSet(viewsets.ModelViewSet):
    model = Place
    serializer_class = PlaceSerializer
    authentication_classes = [TokenAuthentication]
    permission_classes = (IsAuthenticated,)

router = routers.DefaultRouter()
router.register(r'place', PlaceViewSet)
