from django.conf.urls import patterns, include, url
from django.conf import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from place.views import UserDetailView
from django.conf.urls.static import static
from place.views import PlaceListView, get_token
from api import router
urlpatterns = patterns(
    '',
    # Examples:
    url(r'^$', PlaceListView.as_view(), name='home'),
    url(r'^get_token/$', get_token, name='get_token'),
    url(r'^api/', include(router.urls)),
    url(r'^traquenard/', include('place.urls')),
    url(r'^accounts/', include('registration.backends.default.urls')),
    url(r'^accounts/profile', UserDetailView.as_view(),name='profile'),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
