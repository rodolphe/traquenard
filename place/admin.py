from django.contrib.gis import admin as geo_admin
from django.contrib import admin
from models import GeoAdress, Place

geo_admin.site.register(GeoAdress, geo_admin.GeoModelAdmin)
admin.site.register(Place)
