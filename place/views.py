from django.conf import settings
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView
from .models import Place, Image, GeoAdress
from django.contrib.auth.models import User
from django.shortcuts import redirect
from .forms import PlaceForm, ImageForm
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.contrib.gis.measure import D
from django.http import HttpResponse
from django.contrib.auth import authenticate
from rest_framework.authtoken.models import Token
from django.views.decorators.csrf import csrf_exempt

import json

class ProtectedView(object):
    """
    A mixin to decorate each view with login_required.
    """
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProtectedView, self).dispatch(*args, **kwargs)


class PlaceListView(ProtectedView, ListView):
    """
    The Base ListView for places
    """
    model = Place
    paginate_by = settings.PAGINATED_BY


class MyPlaceView(PlaceListView):
    """
    Return places created by the request.user
    """

    def get_queryset(self, *args, **kwargs):
        qs = super(MyPlaceView, self).get_queryset(*args, **kwargs)
        return qs.filter(created_by=self.request.user)

    def get_context_data(self, *args, **kwargs):
        context = super(MyPlaceView, self).get_context_data(*args,
                                                             **kwargs
                                                             )
        context["title"] = "Your Places"
        return context


class PlaceByType(PlaceListView):
    """
    Return places matching a PlaceType
    """
    def get_queryset(self, *args, **kwargs):
        qs = super(PlaceByType, self).get_queryset(*args, **kwargs)
        return qs.filter(place_type__slug=self.kwargs['type'])

    def get_context_data(self, *args, **kwargs):
        context = super(PlaceByType, self).get_context_data(*args,
                                                             **kwargs
                                                             )
        context["title"] = "{0} Places".format(self.kwargs['type'])
        return context


class PlaceDetailView(ProtectedView, DetailView):
    """
    Base Place Detail view
    """
    model = Place

    def get_context_data(self, *args, **kwargs):
        context = super(PlaceDetailView, self).get_context_data(*args, **kwargs)
        near = GeoAdress.objects.filter(
            geom__distance_lte=(
                context['object'].geoadress.geom, D(m=1000)
                )
            ).exclude(place=context['object']).distance(context['object'].geoadress.geom)[:5]

        context['near'] = []
        for obj in near:
            plce = obj.place
            plce.distance = int(obj.distance.m)
            context['near'].append(plce)
        return context



class PlaceCreateView(ProtectedView, CreateView):
    """
    Create New Place. Set the creator of the place as request.user
    """
    form_class = PlaceForm
    model = Place

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(PlaceCreateView, self).form_valid(form)


class ImageCreateView(ProtectedView, CreateView):
    """
    Create an Image for a Place
    """
    form_class = ImageForm
    model = Image

    def form_valid(self, form):
        form.instance.place = Place.objects.get(slug=self.kwargs['slug'])
        return super(ImageCreateView, self).form_valid(form)

    def get_success_url(self):
        return Place.objects.get(slug=self.kwargs['slug']).get_absolute_url()


class PlaceUpdateView(ProtectedView, UpdateView):
    """
    Update a Place
    """
    form_class = PlaceForm
    model = Place


class UserDetailView(ProtectedView, DetailView):
    """
    The Profile of a User
    """
    def get_object(self, *args, **kwargs):
        return self.request.user

    def get_context_data(self, *args, **kwargs):
        context = super(
            UserDetailView, self).get_context_data(*args, **kwargs)

        context["user_places"] = Place().user_places(context["object"])
        context["user_liked_places"] = Place().user_liked_places(
            context["object"])
        return context


class UserDetailViewByUserName(UserDetailView):

    def get_object(self, *args, **kwargs):
        return User.objects.get(username=self.kwargs['username'])


@login_required
def thumb_up(request, slug):
    place = Place.objects.get(slug=slug)
    place.likes.add(request.user)
    return redirect(place.get_absolute_url())


@login_required
def thumb_down(request, slug):
    place = Place.objects.get(slug=slug)
    place.dislikes.add(request.user)
    return redirect(place.get_absolute_url())

@csrf_exempt
def get_token(request):
    """
    authenticate a user and return the authentication token as a json object
    """
    if request.method == "POST":
        username = request.POST.get('username', None)
        password = request.POST.get('password', None)
        if not username or not password:
            return HttpResponse('Unauthorized', status=401)
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                try:
                    token = Token.objects.get(user=user)
                except:
                    token = Token.objects.create(user=user)
                return HttpResponse(json.dumps({'token': token.key}), content_type="application/json")
    return HttpResponse('Unauthorized', status=401)
