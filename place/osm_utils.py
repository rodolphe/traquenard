import requests


def get_geo_text(place):
    url = "http://nominatim.openstreetmap.org/search"
    params = {'polygon_text': 1,
              'format': 'json',
              'q': place.address}
    response = requests.get(url, params=params).json()
    
    if response:
        response = response[0]
        return {'lon': response['lon'],
                'lat':response['lat'],
                'geotext': response['geotext']
                }
    else:
        print requests.get(url, params=params).__dict__
